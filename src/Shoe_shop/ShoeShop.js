import React, { Component } from "react";
import { shoeArr } from "./data_shoe_shop";
import ItemShoe from "./ItemShoe";
import TableGioHang from "./TableGioHang";
export default class ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };
  renderShoe = () => {
    return this.state.shoeArr.map((item) => {
      return (
        <ItemShoe
          hanldeAddTocard={this.hanldeAddTocard}
          shoeData={item}
          key={item.id}
        />
      );
    });
  };
  hanldeAddTocard = (sp) => {
    // let cloneGioHang = [...this.state.gioHang, shoe];
    // this.setState({ gioHang: cloneGioHang });
    // console.log("yes");
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === sp.id;
    });
    let cloneGioHang = [...this.state.gioHang];

    if (index === -1) {
      let newSp = { ...sp, quantity: 1 };
      cloneGioHang.push(newSp);
    } else {
      cloneGioHang[index].quantity++;
    }
    this.setState({
      gioHang: cloneGioHang,
    });
  };

  hanlderemoveShoe = (idShoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };
  hanldeThemGiam = (idShoe, step) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang[index].quantity = cloneGioHang[index].quantity + step;
    if (cloneGioHang[index].quantity === 0) {
      cloneGioHang.splice(index, 1);
    }
    this.setState({ gioHang: cloneGioHang });

    console.log("index: ", index);
  };
  render() {
    console.log(this.state.gioHang);

    return (
      <div className="container py-5">
        {this.state.gioHang.length > 0 && (
          <TableGioHang
            hanldeThemGiam={this.hanldeThemGiam}
            hanlderemoveShoe={this.hanlderemoveShoe}
            gioHang={this.state.gioHang}
          />
        )}
        <div className="row">{this.renderShoe()}</div>
      </div>
    );
  }
}
